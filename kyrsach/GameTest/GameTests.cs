﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CourseWork;
using OpenTK;
using System.Collections.Generic;

namespace GameTest
{
    [TestClass]
    public class GameTests
    {
        /// <summary>
        /// Игровой функционал
        /// </summary>
        GameInteractions gameInteractions = new GameInteractions();
        /// <summary>
        /// Лист объектов
        /// </summary>
        List<GameObject> obj = new List<GameObject>();
        /// <summary>
        /// Игрок 1
        /// </summary>
        Player player1;
        /// <summary>
        /// Игрок 2
        /// </summary>
        Player player2;
        /// <summary>
        /// Приз
        /// </summary>
        Prizes prizes;
        /// <summary>
        /// Конструктор
        /// </summary>
        public GameTests()
        {
            int W = 40;
            int H = 30;
            Vector2 position = new Vector2(600, 10);
            obj.Add(player1 = new Player(W, H, position));
            obj.Add(player2 = new Player(W, H, position));

        }
        /// <summary>
        /// Проверка коллизии игрока с игроком
        /// </summary>
        [TestMethod]
        public void CollisionPlayerWithPlayer()
        {
            
            Player player1 = new Player(40, 30, new Vector2(610, 10));
            Player player2 = new Player(40, 30, new Vector2(600, 10));

            bool expected = true;
            bool result;

            result = player1.Collisions(player2);

            Assert.AreEqual(expected, result);
        }
        /// <summary>
        /// Проверка коллизии игрока со скалой
        /// </summary>
        [TestMethod]
        public void CollisionPlayerWithRock()
        {
            Player player = new Player(40, 30, new Vector2(200, 10));
            Rock rock = new Rock(40, 30, new Vector2(100, 10));

            bool expected = false;
            bool result;

            result = player.Collisions(rock);

            Assert.AreEqual(expected, result);
        }
        /// <summary>
        /// Проверка коллизии пули с игроком
        /// </summary>
        [TestMethod]
        public void CheckCollisionBulletWithPlayer()
        {
            obj.Add(new Bullet(40, 30, new Vector2(200, 10), player1));

            bool expected = false;
 
            gameInteractions.CheckCollisionBullet(obj);

            Assert.AreEqual(expected, player1.CheckCollision);
        }
        /// <summary>
        /// Проверка нанесения урона снарядом
        /// </summary>
        [TestMethod]
        public void CheckCausingDamageBullet()
        {
            obj.Add(new Bullet(40, 30, new Vector2((int)player2.position.X, (int)player2.position.Y), player1));

            int expected = 70;

            gameInteractions.CheckCollisionBullet(obj);

            Assert.AreEqual(expected, player2.Health);
            
        }
        /// <summary>
        /// Проверка нанесения урона торпедой
        /// </summary>
        [TestMethod]
        public void CheckCausingDamageTorpedoes()
        {
            obj.Add( new Torpedoes(40, 30, new Vector2((int)player2.position.X, (int)player2.position.Y), player1));

            int expected = 50;

            gameInteractions.CheckCollisionTorpedoes(obj);

            Assert.AreEqual(expected, player2.Health);

        }

        /// <summary>
        /// Проверка активации приза игроком и поподания игрока в коллизию приза
        /// </summary>
        [TestMethod]
        public void CheckActivatePrizePlayer()
        {
            int countPrize = 0;
            int expected = 140;

            obj.Add(prizes = new Health(40, 30, new Vector2(player1.position.X, player1.position.Y)));

            gameInteractions.ActivatePrizePlayer(obj, ref player1, ref countPrize);

            Assert.AreEqual(expected, player1.Health);

        }
        /// <summary>
        /// Проверка на смерть игрока
        /// </summary>
        [TestMethod]
        public void CheckDeathPlayer()
        {

            int noPlayer = 0;
            player1.Health = 0;
            player2.Health = 0;

            gameInteractions.DeathPlayer(obj);

            Assert.AreEqual(noPlayer, obj.Count);
        }
        /// <summary>
        /// Проверка на снятия декоратора с игрока
        /// </summary>
        [TestMethod]
        public void CheckOffDecorator()
        {
            bool CheckPlayer = true;

            player1 = new UpHealth(player1);
            ((UpHealth)player1).IsDecoration = false;

            gameInteractions.OffDecorator(ref player1, obj);

            bool expected = false;


            if (!(player1 is UpHealth))
            {
                CheckPlayer = false;
            }

            Assert.AreEqual(expected, CheckPlayer);
        }
    }
}
