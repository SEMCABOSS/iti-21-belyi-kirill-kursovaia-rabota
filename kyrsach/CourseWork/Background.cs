﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork
{
    public class Background : GameObject
    {
        /// <summary>
        /// буффер ID вершин текстуры
        /// </summary>
        private int vertexBufferId;

        /// <summary>
        /// массив вершин текстуры
        /// </summary>
        private float[] vertexData;

        /// <summary>
        /// Текстура
        /// </summary>
        private Texture2D texture;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        /// <param name="filename">Путь</param>
        public Background(int width, int height, string filename)
        {
            texture = new Texture2D(filename);
            vertexBufferId = GL.GenBuffer();
            Resize(width, height);
        }

        /// <summary>
        /// Метод для изменения размера текстуры когда изменяется размер окна
        /// </summary>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        public void Resize(int width, int height)
        {
            vertexData = new float[]
            {
                0.0f, 0.0f,0.0f,
                width, 0.0f,0.0f,
                width, height,0.0f,
                0.0f, height,0.0f
            };
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, vertexData.Length * sizeof(float), vertexData, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        /// <summary>
        /// Отрисовка
        /// </summary>
        public override void Draw()
        {
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
            texture.Bind();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.VertexPointer(3, VertexPointerType.Float, 0, 0);
            GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Length);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            texture.Unbind();
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);

        }

        /// <summary>
        /// освобождения памяти
        /// </summary>
        public void Dispose()
        {
            texture.Dispose();
            GL.DeleteBuffer(vertexBufferId);
        }

        /// <summary>
        /// Движение
        /// </summary>
        /// <param name="direction">Позиция</param>
        public override void Move(Vector2 direction)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Коллизия
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>true or false</returns>
        public override bool Collisions(GameObject obj)
        {
            throw new NotImplementedException();
        }
    }
}
