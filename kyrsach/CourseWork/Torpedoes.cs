﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace CourseWork
{
    public class Torpedoes : GameObject
    {
        /// <summary>
        /// буффер ID вершин текстуры
        /// </summary>
        private int vertexBufferId;

        /// <summary>
        /// массив вершин текстуры
        /// </summary>
        private float[] vertexData;

        /// <summary>
        /// Текстура
        /// </summary>
        private Texture2D texture;

        /// <summary>
        /// Корабль который выстрелил
        /// </summary>
        public Player Perent { get; private set; }

        /// <summary>
        /// Угол
        /// </summary>
        public double Angle { get; set; }

        /// <summary>
        /// Урон
        /// </summary>
        public int Damage { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        /// <param name="filename">Путь к тектстуре</param>
        /// <param name="position">Позиция</param>
        /// <param name="perent">Родитель</param>
        /// <param name="angle">Угол</param>
        /// <param name="damage">Урон</param>
        public Torpedoes(int width, int height, string filename, Vector2 position, Player perent, double angle, int damage)
            : base(position, width, height)
        {
            texture = new Texture2D(filename);
            this.position = position;
            W = width;
            H = height;
            Angle = angle;
            R = 15;
            Damage = damage;
            this.Perent = perent;
            vertexBufferId = GL.GenBuffer();
            SetBuffer();
        }

        public Torpedoes(int width, int height, Vector2 position, Player perent)
        {
            this.position = position;
            W = width;
            H = height;
            R = 15;
            Damage = 50;
            Perent = perent;
        }

        /// <summary>
        /// Двожения
        /// </summary>
        /// <param name="direction">Позиция</param>
        public override void Move(Vector2 direction)
        {
            direction = new Vector2(
                (float)(Math.Cos(Angle) * direction.X - Math.Sin(Angle) * direction.Y),
                (float)(Math.Sin(Angle) * direction.X + Math.Cos(Angle) * direction.Y));
            position += direction;
            SetBuffer();
        }

        /// <summary>
        /// Поворот
        /// </summary>
        /// <param name="angle">Угол</param>
        public void Rotate(double angle)
        {
            Angle += angle;
            SetBuffer();
        }

        /// <summary>
        /// Обновления позиции
        /// </summary>
        private void SetBuffer()
        {

            vertexData = new float[]
            {
                0f, 0f, 0.0f,
                W, 0f, 0.0f,
                W, H, 0.0f,
                0f, H,0.0f
            };

            for (int i = 0; i < 4; i++)
            {
                int x = i * 3;
                int y = x + 1;
                vertexData[x] -= (float)W / 2;
                vertexData[y] -= (float)H / 2;
                float vertexX = vertexData[x];
                float vertexY = vertexData[y];

                vertexData[x] = (float)(Math.Cos(Angle) * vertexX - Math.Sin(Angle) * vertexY);
                vertexData[y] = (float)(Math.Sin(Angle) * vertexX + Math.Cos(Angle) * vertexY);
                vertexData[x] += position.X + (float)W / 2;
                vertexData[y] += position.Y + (float)H / 2;
            }
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, vertexData.Length * sizeof(float), vertexData, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }
        /// <summary>
        /// Коллизия
        /// </summary>
        /// <param name="obj">Игровой объект</param>
        /// <returns>true or false</returns>
        public override bool Collisions(GameObject obj)
        {
            float X1 = position.X + W / 2;
            float Y1 = position.Y + H / 2;

            float X2 = obj.position.X + obj.W / 2;
            float Y2 = obj.position.Y + obj.H / 2;


            double d = Math.Sqrt(Math.Pow((X2 - X1), 2) + Math.Pow((Y2 - Y1), 2));

            if (d < R + obj.R)
            {
                if (obj is Player)
                {
                    if (((Player)obj).Armor == 0)
                    {
                        ((Player)obj).Health -= Damage;
                    }
                    else
                    {
                        ((Player)obj).Armor--;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Отрисовка
        /// </summary>
        public override void Draw()
        {
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
            texture.Bind();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.VertexPointer(3, VertexPointerType.Float, 0, 0);
            GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Length);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            texture.Unbind();
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
        }

        /// <summary>
        /// Освобождения памяти
        /// </summary>
        public void Dispose()
        {
            texture?.Dispose();
            GL.DeleteBuffer(vertexBufferId);
        }

    }
}
