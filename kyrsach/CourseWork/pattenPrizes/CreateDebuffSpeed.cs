﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CourseWork
{
    class CreateDebuffSpeed : CreatePrizes
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public CreateDebuffSpeed()
    : base()
        {

        }

        /// <summary>
        /// Создания приза
        /// </summary>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        /// <param name="filename">Путь</param>
        /// <param name="position">Позиция</param>
        /// <returns>Приз</returns>
        public override Prizes Create(int width, int height, string filename, Vector2 position)
        {
            return new DebuffSpeed(width, height, filename, position);
        }
    }
}
