﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CourseWork
{
    public abstract class Prizes : GameObject
    {
        /// <summary>
        /// Буффер Id
        /// </summary>
        private int vertexBufferId;
        /// <summary>
        /// VertexData
        /// </summary>
        private float[] vertexData;
        /// <summary>
        /// Текстура
        /// </summary>
        private Texture2D texture;
        /// <summary>
        /// Угол
        /// </summary>
        public double Angle { get; set; }
        /// <summary>
        /// Проверка приза
        /// </summary>
        public bool CheckPrize { get; set; }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        /// <param name="filename">Путь</param>
        /// <param name="position">Позиция</param>
        public Prizes(int width, int height, string filename, Vector2 position)
            : base(position, width, height)
        {
            texture = new Texture2D(filename);
            CheckPrize = false;
            R = 30;
            vertexBufferId = GL.GenBuffer();
            SetBuffer();
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="width">Ширина/param>
        /// <param name="height">Высота</param>
        /// <param name="position">Позиция</param>
        public Prizes(int width, int height, Vector2 position)
        {
            CheckPrize = false;
            R = 30;

        }
        /// <summary>
        /// Обновления игрока
        /// </summary>
        /// <param name="player">Игрок</param>
        /// <returns>Игрок</returns>
        public abstract Player UpDatePlayer(Player player);


        /// <summary>
        /// Создания приза
        /// </summary>
        /// <param name="obj">J,]trn</param>
        /// <param name="CountPrize">Количество призов</param>
        /// <param name="MAXPRIZE">Макс количество призов</param>
        public static void GetPrizes(List<GameObject> obj, ref int CountPrize , int MAXPRIZE)
        {
            CreatePrizes creator;
            Prizes prizes;
            Random rnd = new Random();

            Vector2 position;
            bool Colision = false;
            while(CountPrize < MAXPRIZE)
            {
                Colision = false;
                position = new Vector2(rnd.Next(10, 1200), rnd.Next(10, 700));
                for (int i = 0; i < obj.Count; i++)
                {
                    if (obj[i].position.X < position.X && obj[i].position.X + obj[i].W > position.X &&
                        obj[i].position.Y < position.Y && obj[i].position.Y + obj[i].H > position.Y)
                    {
                        Colision = true;
                    }
                }

                if (!Colision)
                {

                    switch (rnd.Next(1, 9))
                    {
                        case 1:
                            creator = new CreateHealth();
                            obj.Add(prizes = creator.Create(40, 30, @"content/heart.png", position));
                            break;

                        case 2:
                            creator = new CreateFuel();
                            obj.Add(prizes = creator.Create(50, 40, @"content/fuel.png", position));
                            break;

                        case 3:
                            creator = new CreateSpeed();
                            obj.Add(prizes = creator.Create(40, 30, @"content/speedbutton_120603.png", position));
                            break;

                        case 4:
                            creator = new CreateAmmo();
                            obj.Add(prizes = creator.Create(50, 40, @"content/Ammo1.png", position));
                            break;

                        case 5:
                            creator = new CreateNoAmmo();
                            obj.Add(prizes = creator.Create(50, 40, @"content/NoAmmo1.png", position));
                            break;

                        case 6:
                            creator = new CreateDebuffSpeed();
                            obj.Add(prizes = creator.Create(40, 30, @"content/DebuffSpeed.png", position));
                            break;
                        case 7:
                            creator = new CreateArmor();
                            obj.Add(prizes = creator.Create(50, 40, @"content/Armor.png", position));
                            break;
                        case 8:
                            creator = new CreateDamage();
                            obj.Add(prizes = creator.Create(60, 50, @"content/UpDamage.png", position));
                            break;
    
                    }

                    CountPrize++;
                }

            }
 
        }
        /// <summary>
        /// Обновления позиции
        /// </summary>
        private void SetBuffer()
        {

            vertexData = new float[]
            {
                0.0f+position.X, 0.0f+position.Y,0.0f,
                W+position.X, 0.0f+position.Y,0.0f,
                W+position.X, H+position.Y,0.0f,
                0.0f+position.X, H+position.Y,0.0f
            };


            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, vertexData.Length * sizeof(float), vertexData, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }
        /// <summary>
        /// Отрисовка
        /// </summary>
        public override void Draw()
        {
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
            texture.Bind();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.VertexPointer(3, VertexPointerType.Float, 0, 0);
            GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Length);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            texture.Unbind();
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
        }
        /// <summary>
        /// Освобождения памяти
        /// </summary>
        public void Dispose()
        {
            texture?.Dispose();
            GL.DeleteBuffer(vertexBufferId);
        }

    }
}
