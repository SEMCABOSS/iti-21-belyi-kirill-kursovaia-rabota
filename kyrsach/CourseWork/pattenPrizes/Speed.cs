﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourseWork;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CourseWork
{
    class Speed : Prizes
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        /// <param name="filename">Путь</param>
        /// <param name="position">Позиция</param>
        public Speed(int width, int height, string filename, Vector2 position)
    : base(width, height, filename, position)
        {

        }
        /// <summary>
        /// Обновления игрока
        /// </summary>
        /// <param name="player">Игрок</param>
        /// <returns>Игрок</returns>
        public override Player UpDatePlayer(Player player)
        {
            return player = new ExtraSpeed(player);
        }

        /// <summary>
        /// Коллизия
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>true or false</returns>
        public override bool Collisions(GameObject obj)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Движение
        /// </summary>
        /// <param name="direction">Позиция</param>
        public override void Move(Vector2 direction)
        {
            throw new NotImplementedException();
        }


    }
}
