﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CourseWork
{
    abstract class CreatePrizes
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public CreatePrizes()
        {

        }

        /// <summary>
        /// Создания приза
        /// </summary>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        /// <param name="filename">Путь</param>
        /// <param name="position">Позиция</param>
        /// <returns>Приз</returns>
        abstract public Prizes Create(int width, int height, string filename, Vector2 position);
    }
}
