﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using System.Drawing.Design;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace CourseWork
{
    /// <summary>
    /// Текстура
    /// </summary>
    public class Texture2D : IDisposable
    {
        /// <summary>
        /// ID текстуры
        /// </summary>
        public int ID { get; private set; }
        /// <summary>
        /// Высота текстуры
        /// </summary>
        public int Width { get; private set; }
        /// <summary>
        /// Ширина текстуры
        /// </summary>
        public int Height { get; private set; }
        /// <summary>
        /// Буффер текстуры
        /// </summary>
        public int BufferID { get; private set; }
        /// <summary>
        /// Координаты текстуры
        /// </summary>
        public float[] Coordinates { get; private set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="patch">Путь к файлу</param>
        public Texture2D(string patch)
        {
            Bitmap bitmap = new Bitmap(1, 1);


            if (File.Exists(patch)) //проверка на существования файла
            {
                bitmap = (Bitmap)Image.FromFile(patch);
            }

            bitmap.RotateFlip(RotateFlipType.RotateNoneFlipY); //Переворачивает текстуру что бы не была вверх ногами
            Width = bitmap.Width;
            Height = bitmap.Height;

            BitmapData data = bitmap.LockBits(new Rectangle(0, 0, Width, Height), ImageLockMode.ReadOnly,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);


            ID = GL.GenTexture();//генерация текстуры и сохранения ее ID
            GL.BindTexture(TextureTarget.Texture2D, ID); // забиндить текстуру в 2д по ID

            //Параметры текстуры
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width,
                data.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);

            GL.BindTexture(TextureTarget.Texture2D, 0);
            bitmap.UnlockBits(data);



            Coordinates = new float[] //Текстурные координаты
            {
                0.0f, 1.0f, //ЛВ
                1.0f, 1.0f, //ПВ
                1.0f, 0.0f, //ПН
                0.0f, 0.0f //ЛН
            };


            BufferID = GL.GenBuffer();//генерация буфера

            GL.BindBuffer(BufferTarget.ArrayBuffer, BufferID);//забиндили буффер
            GL.BufferData(BufferTarget.ArrayBuffer, Coordinates.Length * sizeof(float), Coordinates, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        /// <summary>
        /// Биндит текущую текстуру
        /// </summary>
        public void Bind()
        {
            GL.BindTexture(TextureTarget.Texture2D, ID);
            GL.BindBuffer(BufferTarget.ArrayBuffer, BufferID);
            GL.TexCoordPointer(2, TexCoordPointerType.Float, 0, 0);//Описывает массив текстурных координат
        }
        /// <summary>
        /// Отбиндить текстуру
        /// </summary>
        public void Unbind()
        {
            GL.BindTexture(TextureTarget.Texture2D, 0);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }

        //Освобождения памяти
        public void Dispose()
        {
            GL.DeleteBuffer(BufferID);
            GL.DeleteTexture(ID);
        }
    }
}
