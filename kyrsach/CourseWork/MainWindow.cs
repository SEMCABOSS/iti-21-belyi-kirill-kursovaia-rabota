﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;
using OpenTK.Input;
using System.Threading;



namespace CourseWork
{
    public class MainWindow : GameWindow
    {
        /// <summary>
        /// Максимально количество призов
        /// </summary>
        const int MAXPRIZE = 3;
        /// <summary>
        /// количество рпзоив
        /// </summary>
        int CountPrize;
        /// <summary>
        /// Матрица
        /// </summary>
        Matrix4 ortho;
        /// <summary>
        /// Задний фон
        /// </summary>
        Background background;
        /// <summary>
        /// Финал
        /// </summary>
        FinalGame final1;
        /// <summary>
        /// Финал
        /// </summary>
        FinalGame final2;
        /// <summary>
        /// Игрок 1
        /// </summary>
        Player player;
        /// <summary>
        /// Игрок 2
        /// </summary>
        Player player1;
        /// <summary>
        /// Функционал
        /// </summary>
        GameInteractions gameInteractions = new GameInteractions();
        /// <summary>
        /// Лист объектов
        /// </summary>
        List<GameObject> obj = new List< GameObject >();


        

        /// <summary>
        /// /Конструктор
        /// </summary>
        /// <param name="title">Названия окна</param>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        public MainWindow(string title, int width = 1366, int height = 768  )
        {
            Title = title;
            Width = width;
            Height = height;
            WindowBorder = WindowBorder.Hidden;
            WindowState = WindowState.Fullscreen;

            GL.Enable(EnableCap.PointSmooth);
            GL.Enable(EnableCap.LineSmooth);
            GL.Enable(EnableCap.PolygonSmooth);
            GL.Enable(EnableCap.Multisample);
            GL.Enable(EnableCap.Texture2D);
            GL.Enable(EnableCap.AlphaTest);
            GL.Enable(EnableCap.Blend);
            GL.Enable(EnableCap.CullFace);
            GL.BlendFunc(BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);
            GL.FrontFace(FrontFaceDirection.Cw);
            GL.CullFace(CullFaceMode.Back);



            obj.Add(player = new Player(Width / 15, Height / 12, @"content/shipp1.png", new Vector2(600, 10)));
            obj.Add(player1 = new Player(Width / 15, Height / 12, @"content/pirate_ship_00000.png", new Vector2(600, 650)));
            background = new Background(Width, Height, @"content/tex_Water.jpg");

            //Создание скал
            gameInteractions.CreateRocks(obj, Width, Height);

            //Создания приза
            Prizes.GetPrizes(obj, ref CountPrize, MAXPRIZE);

        }



        // Определения размера экрана
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            GL.Viewport(0, 0, Width, Height);
            ortho = Matrix4.CreateOrthographicOffCenter(0, Width, Height, 0, -1, 1);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref ortho);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadIdentity();
            background.Resize(Width, Height);
        }

        /// <summary>
        /// Обновления каждого кадра
        /// </summary>
        /// <param name="e"></param>
        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            KeyboardState keyboardState = Keyboard.GetState();

            if (keyboardState.IsKeyDown(Key.Escape)) Exit();

            //проверка на поподание снаряда
            gameInteractions.CheckCollisionBullet(obj);
            //проверка на попадание торпеды
            gameInteractions.CheckCollisionTorpedoes(obj);
            //Проверка на смерть игрока 
            gameInteractions.DeathPlayer(obj);


            //Движения первого игрока
            gameInteractions.MovePlayer1(player);
            //Проверка на попадание в колизию 1 игрока во 2го
            player.Collisions1(player1);
            //Проверка на попадание в колизию скалы 1 игрока
            gameInteractions.CollisionRocks(obj, player);
            //Проверка на попадание в колизию приза и активирование его 1 игроком
            gameInteractions.ActivatePrizePlayer(obj, ref player, ref CountPrize);


            //Движение 2го игрока
            gameInteractions.MovePlayer2(player1);
            //Проверка на попадание в колизию 2 игрока во 1
            player1.Collisions1(player);
            //Проверка на попадание в колизию скалы 2 игрока
            gameInteractions.CollisionRocks(obj, player1);
            //Проверка на попадание в колизию приза и активирование его 2 игроком
            gameInteractions.ActivatePrizePlayer1(obj, ref player1, ref CountPrize);

            //Стрельба 1 игрока
            gameInteractions.ShootingPlayer(obj, ref player1, e);
            //Стрельба 2 игрока
            gameInteractions.ShootingPlayer1(obj, ref player, e);


            //Отключения декоратора 1 игрока
            gameInteractions.OffDecorator(ref player, obj);
            //Отключения декоратора 2 игрока
            gameInteractions.OffDecorator1(ref player1, obj);


            //Создания приза
            Prizes.GetPrizes(obj, ref CountPrize, MAXPRIZE);

            
        }

        /// <summary>
        /// Обновления окна
        /// </summary>
        /// <param name="e"></param>
        protected override  void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            base.OnRenderFrame(e);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.ClearColor(Color4.Aqua);

            //Отрисовка игры
            gameInteractions.DrawGame(obj, background, player, player1, final1, final2, Width, Height);
            
            GL.LoadIdentity();
            SwapBuffers();
        }
    }
}
