﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Graphics;
using OpenTK.Input;
using CourseWork;

namespace CourseWork
{
    public class GameInteractions
    {
        float time1;
        float time2;
        private object e;
        /// <summary>
        /// Скала
        /// </summary>
        Rock[] rocks = new Rock[10];
        /// <summary>
        /// Лист объектов
        /// </summary>
        List<GameObject> obj = new List<GameObject>();
        /// <summary>
        /// Конструкотор
        /// </summary>
        public GameInteractions()
        {

        }

        /// <summary>
        /// Движение первого игрока
        /// </summary>
        /// <param name="player">Игрок</param>
        public void MovePlayer1(Player player)
        {
            KeyboardState keyboardState = Keyboard.GetState();


            Vector2 translate = Vector2.Zero;

            if (keyboardState.IsKeyDown(Key.Up) && player.Fuel > 0)
            {
                translate.Y += player.Speed.Y;
                player.Fuel -= 0.1f;
            }

            if (keyboardState.IsKeyDown(Key.Down) && player.Fuel > 0)
            {
                translate.Y -= player.Speed.Y;
                player.Fuel -= 0.1f;
            }


            if (keyboardState.IsKeyDown(Key.Left))
            {
                player.Rotate(-0.05);

            }
            if (keyboardState.IsKeyDown(Key.Right))
            {
                player.Rotate(0.05);
            }

            player.Move(translate);     

        }


        /// <summary>
        /// Движение второго игрока
        /// </summary>
        /// <param name="player">Игрок</param>
        public void MovePlayer2(Player player)
        {
            KeyboardState keyboardState = Keyboard.GetState();

            Vector2 translate = Vector2.Zero;

            if (keyboardState.IsKeyDown(Key.W) && player.Fuel > 0)
            {
                translate.Y += player.Speed.Y;
                player.Fuel -= 0.1f;
            }

            if (keyboardState.IsKeyDown(Key.S) && player.Fuel > 0)
            {
                translate.Y -= player.Speed.Y;
                player.Fuel -= 0.1f;
            }

            if (keyboardState.IsKeyDown(Key.A))
            {
                player.Rotate(-0.05);
            }
            if (keyboardState.IsKeyDown(Key.D))
            {
                player.Rotate(0.05);
            }

            player.Move(translate);

        }

        /// <summary>
        /// Стрельба первого игрока
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <param name="player">Игрок</param>
        /// <param name="e"></param>
        public void ShootingPlayer(List<GameObject> obj, ref Player player, FrameEventArgs e)
        {
            KeyboardState keyboardState = Keyboard.GetState();

            time1 += (float)e.Time;
            if (keyboardState.IsKeyDown(Key.F))
            {
                if (player.CountBullet > 0)
                {
                    if (time1 >= 2)
                    {
                        obj.Add(new Bullet(30, 30, @"content/1e808817febb22a.png", new Vector2(player.position.X + 40, player.position.Y + 25), player, player.Angle, player.DamageBullet));

                        obj.Add(new Bullet(30, 30, @"content/1e808817febb22a.png", new Vector2(player.position.X + 40, player.position.Y + 25), player, player.Angle - 3.14, player.DamageBullet));
                        time1 = 0;
                        player.CountBullet -= 1;
                    }
                }
            }

            time2 += (float)e.Time;
            if (keyboardState.IsKeyDown(Key.C))
            {
                if (player.CountBullet > 0)
                {
                    if (time2 >= 10)
                    {
                        obj.Add(new Torpedoes(30, 30, @"content/torpedos.png", new Vector2(player.position.X + 40, player.position.Y + 25), player, player.Angle + 1.57, player.DamageTorpedoes));
                        time2 = 0;
                        player.CountBullet -= 1;
                    }
                }
            }
        }

        /// <summary>
        /// Стрельба второго игрока
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <param name="player">Игрок</param>
        /// <param name="e"></param>
        public void ShootingPlayer1(List<GameObject> obj, ref Player player, FrameEventArgs e)
        {
            KeyboardState keyboardState = Keyboard.GetState();

            time1 += (float)e.Time;
            if (keyboardState.IsKeyDown(Key.Enter))
            {
                if (player.CountBullet > 0)
                {
                    if (time1 >= 2)
                    {
                        obj.Add(new Bullet(30, 30, @"content/1e808817febb22a.png", new Vector2(player.position.X + 40, player.position.Y + 25), player, player.Angle, player.DamageBullet));

                        obj.Add(new Bullet(30, 30, @"content/1e808817febb22a.png", new Vector2(player.position.X + 30, player.position.Y + 25), player, player.Angle - 3.14, player.DamageBullet));
                        
                        time1 = 0;
                        player.CountBullet -= 1;
                    }
                }
            }

            time2 += (float)e.Time;
            if (keyboardState.IsKeyDown(Key.ControlRight))
            {
                if (player.CountBullet > 0)
                {
                    if (time2 >= 10)
                    {
                        obj.Add(new Torpedoes(30, 30, @"content/torpedos.png", new Vector2(player.position.X + 40, player.position.Y + 25), player, player.Angle + 1.57, player.DamageTorpedoes));
                        time2 = 0;
                        player.CountBullet -= 1;
                    }
                }
            }
        }

        /// <summary>
        /// Отключения декоратора первого игрока
        /// </summary>
        /// <param name="player">Игрок</param>
        /// <param name="obj">Объект</param>
        public void OffDecorator(ref Player player, List<GameObject> obj)
        {
            if (player is Decorator && !((Decorator)player).IsDecoration)
            {
                obj.Remove(player);
                player = ((Decorator)player).GetOldPlayer();
                obj.Insert(0, player);
            }
        }
        /// <summary>
        /// Отключения декоратора второго игрока
        /// </summary>
        /// <param name="player1">Игрок</param>
        /// <param name="obj">Объект</param>
        public void OffDecorator1(ref Player player1, List<GameObject> obj)
        {
            if (player1 is Decorator && !((Decorator)player1).IsDecoration)
            {
                obj.Remove(player1);
                player1 = ((Decorator)player1).GetOldPlayer();
                obj.Insert(1, player1);
            }
        }

        /// <summary>
        /// Отрисовка игры
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <param name="background">Задний фон</param>
        /// <param name="player">Игрок</param>
        /// <param name="player1">Игрок</param>
        /// <param name="final1">Финальное изображения</param>
        /// <param name="final2">Финальное изображения</param>
        /// <param name="W">Ширина</param>
        /// <param name="H">Высота</param>
        public void DrawGame(List<GameObject> obj, Background background, Player player, Player player1, FinalGame final1, FinalGame final2,
            int W, int H)
        {
            if (player.Health > 0 && player1.Health > 0)
            {
                background.Draw();

                for (int i = 0; i < obj.Count; i++)
                {
                    obj[i].Draw();

                    if (obj[i] is Bullet)
                    {

                        obj[i].Move(new Vector2(3, 0));

                    }

                    if (obj[i] is Torpedoes)
                    {
                        obj[i].Move(new Vector2(1.3f, 0));
                    }
                }
            }

            if (player1.Health <= 0)
            {
                final1 = new FinalGame(W, H, @"content/FinalPlayer2.png");
                final1.Draw();
            }

            if (player.Health <= 0)
            {
                final2 = new FinalGame(W, H, @"content/FinalPlayer1.png");
                final2.Draw();
            }


        }

        /// <summary>
        /// Активация призов первым игроком
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <param name="player">Игрок</param>
        /// <param name="CountPrize">Колличество призов</param>
        public void ActivatePrizePlayer(List<GameObject> obj, ref Player player, ref int CountPrize)
        {
            for (int i = 0; i < obj.Count; i++)
            {
                if (obj[i] is Prizes && player.Collisions((Prizes)obj[i]))
                {

                    obj.Remove(player);
                    i--;
                    player = ((Prizes)obj[i]).UpDatePlayer(player);
                    obj.Remove(obj[i]);
                    obj.Insert(0, player);
                    CountPrize--;
                }
            }
        }

        /// <summary>
        /// Активация призов вторым ироком
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <param name="player">Игрок</param>
        /// <param name="CountPrize">Количество призов</param>
        public void ActivatePrizePlayer1(List<GameObject> obj, ref Player player, ref int CountPrize)
        {
            for (int i = 0; i < obj.Count; i++)
            {
                if (obj[i] is Prizes && player.Collisions((Prizes)obj[i]))
                {

                    obj.Remove(player);
                    i--;
                    player = ((Prizes)obj[i]).UpDatePlayer(player);
                    obj.Remove(obj[i]);
                    obj.Insert(1, player);
                    CountPrize--;
                }
            }
        }

        /// <summary>
        /// Проверка коллизии торпеты
        /// </summary>
        /// <param name="obj">Объект</param>
        public void CheckCollisionTorpedoes(List<GameObject> obj)
        {
            for (int i = 0; i < obj.Count; i++)
            {
                for (int j = 0; j < obj.Count; j++)
                {
                    if (obj[i] is Torpedoes)
                    {
                        if (i != j && obj[j] != ((Torpedoes)obj[i]).Perent)
                        {
                            obj[i].CheckCollision = obj[i].CheckCollision || obj[i].Collisions(obj[j]);
                        }
                    }
                }

                if (obj[i].CheckCollision)
                {
                    obj.Remove(obj[i]);
                    i--;
                }

            }
        }

        /// <summary>
        /// Проверка коллизии снаряда
        /// </summary>
        /// <param name="obj">Объект</param>
        public void CheckCollisionBullet(List<GameObject> obj)
        {
            for (int i = 0; i < obj.Count; i++)
            {
                for (int j = 0; j < obj.Count; j++)
                {
                    if (obj[i] is Bullet)
                    {
                        if (i != j && obj[j] != ((Bullet)obj[i]).Perent)
                        {
                            if (obj[j] is Player)
                            {
                                obj[i].CheckCollision = obj[i].CheckCollision || obj[i].Collisions(obj[j]);
                            }
                        }
                    }
                }

                if (obj[i].CheckCollision)
                {
                    obj.Remove(obj[i]);
                    i--;
                }

            }

        }

        /// <summary>
        /// Смерть игрока
        /// </summary>
        /// <param name="obj">Объект</param>
        public void DeathPlayer(List<GameObject> obj)
        {
            for (int i = 0; i < obj.Count; i++)
            {
                if (obj[i] is Player && ((Player)obj[i]).Health <= 0)
                {
                    obj.Remove(obj[i]);
                    i--;
                }
            }
        }

        /// <summary>
        /// Создания скал
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <param name="W">Ширина</param>
        /// <param name="H">Высота</param>
        public void CreateRocks(List<GameObject> obj, int W, int H)
        {
            Random rnd = new Random();
            for (int i = 0; i < rocks.Length; i++)
            {
                obj.Add(rocks[i] = new Rock(W / 15, H / 12, @"content/RockPile.png", new Vector2(rnd.Next(0, 1200), rnd.Next(0, 700))));
            }

        }

        /// <summary>
        /// Коллизия скал с игроком
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <param name="player">Игрок</param>
        public void CollisionRocks(List<GameObject> obj, Player player)
        {
            for (int i = 0; i < obj.Count; i++)
            {
                if (obj[i] is Rock)
                {
                    player.Collisions1(obj[i]);
                }

            }
        }
    }
}
