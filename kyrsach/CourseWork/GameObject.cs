﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CourseWork
{

    public abstract class GameObject
    {
        /// <summary>
        /// Позиция
        /// </summary>
        public virtual Vector2 position { get; set; }

        /// <summary>
        /// Высота и ширна
        /// </summary>
        public int W, H;
        /// <summary>
        /// Радиус
        /// </summary>
        public float R { get; set; }
        /// <summary>
        /// Путь к текстуре
        /// </summary>
        public string Filename{ get; set; }
        /// <summary>
        /// Проверка на коллизию
        /// </summary>
        public bool CheckCollision { get; set; }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="position">Позиция</param>
        /// <param name="W">Ширина</param>
        /// <param name="H">Высота</param>
        public GameObject(Vector2 position, int W, int H)
        {
            this.position = position;
            this.W = W;
            this.H = H;
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        protected GameObject()
        {

        }

        /// <summary>
        /// Отрисовка
        /// </summary>
        public abstract void Draw();
        /// <summary>
        /// Движения
        /// </summary>
        /// <param name="direction"></param>
        public abstract void Move(Vector2 direction);
        /// <summary>
        /// Коллизия
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>true or false</returns>
        public abstract bool Collisions(GameObject obj);
    }
}
