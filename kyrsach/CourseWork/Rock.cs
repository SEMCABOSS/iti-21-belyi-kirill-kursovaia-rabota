﻿using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace CourseWork
{
    public class Rock : GameObject
    {
        /// <summary>
        /// буффер ID вершин текстуры
        /// </summary>
        private int vertexBufferId;

        /// <summary>
        /// массив вершин текстуры
        /// </summary>
        private float[] vertexData;

        /// <summary>
        /// Текстура
        /// </summary>
        private Texture2D texture;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        /// <param name="filename">Путь</param>
        /// <param name="position">Пзиция</param>
        public Rock(int width, int height, string filename, Vector2 position)
            : base(position, width, height)
        {
            texture = new Texture2D(filename);
            this.position = position;
            W = width;
            H = height;
            R = 35;
            vertexBufferId = GL.GenBuffer();
            SetBuffer();
        }

        public Rock(int width, int height, Vector2 position)
        {
            this.position = position;
            W = width;
            H = height;
            R = 35;
        }
        /// <summary>
        /// Изменения позиции
        /// </summary>
        private void SetBuffer()
        {

            vertexData = new float[]
            {
                0.0f+position.X, 0.0f+position.Y,0.0f,
                W+position.X, 0.0f+position.Y,0.0f,
                W+position.X, H+position.Y,0.0f,
                0.0f+position.X, H+position.Y,0.0f
            };


            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, vertexData.Length * sizeof(float), vertexData, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }
        /// <summary>
        /// Коллизия
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>true or false</returns>
        public override bool Collisions(GameObject obj)
        {

            float X1 = position.X + W / 2;
            float Y1 = position.Y + H / 2;

            float X2 = obj.position.X + obj.W / 2;
            float Y2 = obj.position.Y + obj.H / 2;



            double d = Math.Sqrt(Math.Pow((X2 - X1), 2) + Math.Pow((Y2 - Y1), 2));

            if (d > R + obj.R)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Отрисовка
        /// </summary>
        public override void Draw()
        {
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
            texture.Bind();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.VertexPointer(3, VertexPointerType.Float, 0, 0);
            GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Length);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            texture.Unbind();
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
        }

        /// <summary>
        /// Освобождения памяти
        /// </summary>
        public void Dispose()
        {
            texture?.Dispose();
            GL.DeleteBuffer(vertexBufferId);
        }
        /// <summary>
        /// Движение
        /// </summary>
        /// <param name="direction"></param>
        public override void Move(Vector2 direction)
        {
            throw new NotImplementedException();
        }
    }
}
