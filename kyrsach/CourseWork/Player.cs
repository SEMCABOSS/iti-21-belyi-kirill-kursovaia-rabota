﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace CourseWork
{
    public class Player : GameObject
    {
        /// <summary>
        /// буффер ID вершин текстуры
        /// </summary>
        private int vertexBufferId;

        /// <summary>
        /// массив вершин текстуры
        /// </summary>
        private float[] vertexData;

        /// <summary>
        /// Текстура
        /// </summary>
        private Texture2D texture;
        /// <summary>
        /// Топливо
        /// </summary>
        public virtual float Fuel { get; set; }
        /// <summary>
        /// Скорость
        /// </summary>
        public virtual Vector2 Speed { get; set; }
        /// <summary>
        /// Здоровье
        /// </summary>
        public virtual int Health { get; set; }
        /// <summary>
        /// Угол
        /// </summary>
        public virtual double Angle { get; set; }
        /// <summary>
        /// Запас патрон
        /// </summary>
        public virtual int CountBullet { get; set; }
        /// <summary>
        /// Броня
        /// </summary>
        public virtual int Armor { get; set; }
        /// <summary>
        /// Урон снаряда
        /// </summary>
        public virtual int DamageBullet { get; set; }
        /// <summary>
        /// Урон торпеды
        /// </summary>
        public virtual int DamageTorpedoes { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        /// <param name="filename">Путь</param>
        /// <param name="positionint">Позиция</param>
        public Player( int  width, int height, string filename, Vector2 positionint)
            : base (positionint, width, height)
        {
            Filename = filename;
            texture = new Texture2D(filename);
            Speed = new Vector2(0, 1.6f); 
            R = 25;
            Health = 100;
            CountBullet = 20;
            Fuel = 300;
            Armor = 0;
            DamageBullet = 30;
            DamageTorpedoes = 50;
            vertexBufferId = GL.GenBuffer();

            SetBuffer();
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высотаparam>
        /// <param name="positionint">Позиция</param>
        public Player(int width, int height, Vector2 positionint)
        {
            Speed = new Vector2(0, 1.6f);
            R = 25;
            Health = 100;
            CountBullet = 20;
            Fuel = 300;
        }
        /// <summary>
        /// Конструктор
        /// </summary>
        protected Player ()
            :base()
        {

        }

        /// <summary>
        /// Движения
        /// </summary>
        /// <param name="direction">Позиция</param>
        public override void Move(Vector2 direction)
        {
            direction = new Vector2(
                (float)(Math.Cos(Angle) * direction.X - Math.Sin(Angle) * direction.Y),
                (float)(Math.Sin(Angle) * direction.X + Math.Cos(Angle) * direction.Y));
            Vector2 newPos = position + direction;
            if (newPos.X > 0 && newPos.X + W < 1366 && newPos.Y > 0 && newPos.Y + H < 768)
            {
                position += direction;
                SetBuffer();
            }


          
        }
        /// <summary>
        /// Поворот
        /// </summary>
        /// <param name="angle">Угол</param>
        public virtual void Rotate(double angle)
        {
            Angle += angle;
            SetBuffer();
        }
        /// <summary>
        /// Обновления позиции
        /// </summary>
        private void SetBuffer()
        {
            vertexData = new float[]
            {
                0f, 0f, 0.0f,
                W, 0f, 0.0f,
                W, H, 0.0f,
                0f, H,0.0f
            };

            for (int i = 0; i < 4; i++)
            {
                int x = i * 3;
                int y = x + 1;
                vertexData[x] -= (float)W / 2;
                vertexData[y] -= (float)H / 2;
                float vertexX = vertexData[x];
                float vertexY = vertexData[y];
                
                vertexData[x] = (float) (Math.Cos(Angle) * vertexX - Math.Sin(Angle) * vertexY);
                vertexData[y] = (float) (Math.Sin(Angle) * vertexX + Math.Cos(Angle) * vertexY);
                vertexData[x] += position.X + (float) W / 2;
                vertexData[y] += position.Y + (float) H / 2;
            }

            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, vertexData.Length * sizeof(float), vertexData, BufferUsageHint.StaticDraw);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
        }
        /// <summary>
        /// Коллизия
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>true or false</returns>
        public override bool Collisions(GameObject obj)
        {

            float X1 = position.X + W / 2;
            float Y1 = position.Y + H / 2;

            float X2 = obj.position.X + obj.W / 2;
            float Y2 = obj.position.Y + obj.H / 2;


            double d = Math.Sqrt(Math.Pow((X2 - X1), 2) + Math.Pow((Y2 - Y1), 2));

            if (d < R + obj.R)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// Коллизия
        /// </summary>
        /// <param name="obj">Объект</param>
        public virtual void Collisions1(GameObject obj)
        {
            float X1 =  position.X;
            float Y1 = position.Y;

            float X2 = obj.position.X;
            float Y2 = obj.position.Y;


            double d = Math.Sqrt(Math.Pow((X2 - X1), 2) + Math.Pow((Y2 - Y1), 2));

            if (d < R + obj.R)
            {
                Vector2 dir = position - obj.position;
                dir.Normalize();
                dir *= (float)(R + obj.R - d);
                position += dir;
            }
        }

        /// <summary>
        /// Отрисовка
        /// </summary>
        public override void Draw()
        {
            GL.EnableClientState(ArrayCap.VertexArray);
            GL.EnableClientState(ArrayCap.TextureCoordArray);
            texture.Bind();
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertexBufferId);
            GL.VertexPointer(3, VertexPointerType.Float, 0, 0);
            GL.DrawArrays(PrimitiveType.Quads, 0, vertexData.Length);
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            texture.Unbind();
            GL.DisableClientState(ArrayCap.VertexArray);
            GL.DisableClientState(ArrayCap.TextureCoordArray);
        }


        /// <summary>
        /// Освобождения памяти
        /// </summary>
        public virtual void Dispose()
        {
            texture?.Dispose();
            GL.DeleteBuffer(vertexBufferId);
        }
    }
}
