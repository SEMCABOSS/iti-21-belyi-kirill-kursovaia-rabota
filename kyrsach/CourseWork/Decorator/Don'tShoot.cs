﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CourseWork
{
    class Don_tShoot : Decorator
    {
        /// <summary>
        /// Колличество старых пуль
        /// </summary>
        private int oldCountBullet;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="player">Игрок</param>
        public Don_tShoot(Player player)
    : base(player)
        {
            oldCountBullet = player.CountBullet;
            player.CountBullet = 0;
        }
        /// <summary>
        /// Вернуть старого игрока
        /// </summary>
        /// <returns>Игрок</returns>
        public override Player GetOldPlayer()
        {
            player.CountBullet = oldCountBullet;
            return base.GetOldPlayer();
        }
    }
}
