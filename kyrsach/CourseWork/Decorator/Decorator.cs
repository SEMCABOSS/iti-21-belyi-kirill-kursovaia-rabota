﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;


namespace CourseWork
{
   public abstract class Decorator : Player
    {
        /// <summary>
        /// Экземпляр игрока
        /// </summary>
        public Player player;

        /// <summary>
        /// Время
        /// </summary>
        protected int timeSet;
        
        /// <summary>
        /// Время действия
        /// </summary>
        protected int Duration;

        /// <summary>
        /// Проверка декоратора
        /// </summary>
        public bool IsDecoration { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="player"></param>
        public Decorator(Player player)
            : base ()
        {
            this.player = player;
            IsDecoration = true;
            timeSet = DateTime.Now.Second;
            Duration = 5;
        }
        /// <summary>
        /// Скорость
        /// </summary>
        public override Vector2 Speed { get => player.Speed; set => player.Speed = value; }
        /// <summary>
        /// Здоровье
        /// </summary>
        public override int Health { get => player.Health; set => player.Health = value; }
        /// <summary>
        /// Колличество пуль
        /// </summary>
        public override int CountBullet { get => player.CountBullet; set => player.CountBullet = value; }
        /// <summary>
        /// Позиция
        /// </summary>
        public override Vector2 position { get => player.position; set => player.position = value; }
        /// <summary>
        /// Топливо
        /// </summary>
        public override float Fuel { get => player.Fuel; set => player.Fuel = value; }
        /// <summary>
        /// Угол
        /// </summary>
        public override double Angle { get => player.Angle; set => player.Angle = value; }
        /// <summary>
        /// Броня
        /// </summary>
        public override int Armor { get => player.Armor; set => player.Armor = value; }
        /// <summary>
        /// Урон торпеды
        /// </summary>
        public override int DamageTorpedoes { get => player.DamageTorpedoes; set => player.DamageTorpedoes = value; }
        /// <summary>
        /// Урон пули
        /// </summary>
        public override int DamageBullet { get => player.DamageBullet; set => player.DamageBullet = value; }

        /// <summary>
        /// Возращения старого игрока
        /// </summary>
        /// <returns>Игрок</returns>
        public virtual Player GetOldPlayer()
        {
            return player;
        }
        /// <summary>
        /// Коллизия
        /// </summary>
        /// <param name="obj">Объект</param>
        /// <returns>true or false</returns>
        public override bool Collisions(GameObject obj)
        {
            return player.Collisions(obj);
        }

        /// <summary>
        /// Коллизия
        /// </summary>
        /// <param name="obj">Объект</param>
        public override void Collisions1(GameObject obj)
        {
            player.Collisions1(obj);
        }
        /// <summary>
        /// Освобождения памяти
        /// </summary>
        public override void Dispose()
        {
            player.Dispose();
        }
        /// <summary>
        /// Отрисовка
        /// </summary>
        public override void Draw()
        {
            if (timeSet + Duration < DateTime.Now.Second)
            {
                IsDecoration = false;
            }

            player.Draw();
        }
        /// <summary>
        /// Движения
        /// </summary>
        /// <param name="direction"></param>
        public override void Move(Vector2 direction)
        {
            player.Move(direction);
        }
        /// <summary>
        /// Поворот
        /// </summary>
        /// <param name="angle">Угол</param>
        public override void Rotate(double angle)
        {
            player.Rotate(angle);
        }
    }
}
