﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CourseWork
{
    class ExtraSpeed : Decorator
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="player">Игрок</param>
        public ExtraSpeed(Player player)
            : base(player)
        {
            player.Speed = new Vector2(0,2.5f); 
        }
        /// <summary>
        /// Вернуть старого игрока
        /// </summary>
        /// <returns>Игрок</returns>
        public override Player GetOldPlayer()
        {
            player.Speed = new Vector2(0, 1.6f);
            return base.GetOldPlayer();
        }
    }
}
