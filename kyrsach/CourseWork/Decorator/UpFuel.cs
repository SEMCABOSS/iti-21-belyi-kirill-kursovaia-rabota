﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;


namespace CourseWork
{
    class UpFuel : Decorator
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="player">Игрок</param>
        public UpFuel(Player player)
        : base(player)
        {
            player.Fuel += 100;
        }
        /// <summary>
        /// Отрисовка
        /// </summary>
        public override void Draw()
        {
            IsDecoration = false;
        }
        /// <summary>
        /// Вернуть старого игрока
        /// </summary>
        /// <returns>Игрок</returns>
        public override Player GetOldPlayer()
        {
            return player;
        }
    }

}