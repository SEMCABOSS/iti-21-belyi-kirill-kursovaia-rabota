﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CourseWork
{
   public class UpHealth : Decorator
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="player">Игрок</param>
        public UpHealth(Player player)
        : base(player)
        {
            player.Health += 40;
        }
        /// <summary>
        /// Отрисовка
        /// </summary>
        public override void Draw()
        {
            IsDecoration = false;
        }
        /// <summary>
        /// Вернуть старого игрока
        /// </summary>
        /// <returns>Игрок</returns>
        public override Player GetOldPlayer()
        {
            return player;
        }
    }
}
