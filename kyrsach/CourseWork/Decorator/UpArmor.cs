﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseWork
{
    class UpArmor : Decorator
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="player">Игрок</param>
        public UpArmor(Player player)
        : base(player)
        {
            player.Armor = 2;
        }
        /// <summary>
        /// Отрисовка
        /// </summary>
        public override void Draw()
        {
            IsDecoration = false;
        }
        /// <summary>
        /// Вернуть старого игрока
        /// </summary>
        /// <returns>Игрок</returns>
        public override Player GetOldPlayer()
        {
            return player;
        }
    }
}
