﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace CourseWork
{
    class ExtraDamage : Decorator
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="player">Игрок</param>
        public ExtraDamage(Player player)
        : base(player)
        {
            player.DamageBullet = 50;
            player.DamageTorpedoes = 100;
        }
        /// <summary>
        /// Вернуть старого игрока
        /// </summary>
        /// <returns>Игрок</returns>
        public override Player GetOldPlayer()
        {
            player.DamageBullet = 30;
            player.DamageTorpedoes = 50;
            return base.GetOldPlayer();
        }
    }
}
