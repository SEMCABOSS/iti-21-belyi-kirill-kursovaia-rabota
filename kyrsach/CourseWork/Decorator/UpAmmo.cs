﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;


namespace CourseWork
{
    class UpAmmo : Decorator
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="player">Игрок</param>
        public UpAmmo(Player player)
        : base(player)
        {
            player.CountBullet += 10;
        }
        /// <summary>
        /// Отрисовка
        /// </summary>
        public override void Draw()
        {
            IsDecoration = false;
        }
        /// <summary>
        /// Вернуть старого игрока
        /// </summary>
        /// <returns>Игрок</returns>
        public override Player GetOldPlayer()
        {
            return player;
        }
    }

}
